# Verdieping SCSS

SCSS staat voor "Sassy CSS" en het is eigenlijk een uitbreiding van gewoon CSS (Cascading Style Sheets). Het voegt extra functionaliteit en mogelijkheden toe aan CSS, waardoor het gemakkelijker wordt om stijlen voor websites te schrijven en te beheren.

In plaats van de traditionele CSS-syntaxis, die soms herhaaldelijk kan zijn, maakt SCSS gebruik van een meer gestructureerde en programmeerachtige syntaxis. Hierdoor kun je variabelen gebruiken om waarden op te slaan, herbruikbare mixins maken voor herhaalde stijlpatronen, en nestings gebruiken om de structuur van je stijlregels duidelijker te maken.

```scss
// Voorbeeld van nesting met SCSS. In dit geval ziet onze CSS er zo uit:
#header p {
    font-size: 18px;
}

#header img {
    max-width: 100%;
}

#header h1 {
    font-size: 30px;
}

// In SCSS kan dit veel makkelijker, namelijk met nesting:
#header {
    p {
        font-size: 18px;
    }

    img {
        max-width: 100%;
    }

    h1 {
        font-size: 30px;
    }
}
```

Omdat SCSS een uitbereiding is van CSS kun je deze niet zomaar gebruiken in de HTML. SCSS moet eerst worden vertaald naar CSS, dat kan met SASS. Gelukkig werken wij met React, React is zelf slim genoeg om dit te vertalen, maar je moet wel SASS installeren via npm (`npm i sass`).

Een ander voordeel, wat je op het examen vaak zult gebruiken zijn variabelen. Met variabelen kun je een wijziging doorvoeren op verschillende locaties. Variabelen werken eigenlijk het zelfde als in een programmeertaal.

```scss
// Definieer een SCSS-variabele voor de primaire kleur
$primary-color: #007bff;

// Gebruik de variabele in je stijlregels
.button {
  background-color: $primary-color;
  color: white;
  padding: 10px 20px;
}
```

Bootstrap maakt uitgebreid gebruik van SCSS-variabelen, waardoor je de look en feel van je Bootstrap-componenten gemakkelijk kunt aanpassen door slechts enkele variabelen te wijzigen.

Bijvoorbeeld, Bootstrap heeft een reeks variabelen voor kleuren, zoals `$primary`, `$secondary`, etc. Als je de primaire kleur wilt aanpassen, hoef je alleen de waarde van de `$primary`-variabele te wijzigen, en dit zal automatisch van invloed zijn op alle plaatsen waar deze kleur wordt gebruikt in Bootstrap-componenten. Dit maakt het aanpassen van de stijl van je app eenvoudiger en consistenter. Andere voorbeelden van variabele zijn `$card-border-width` of `$base-bg`.